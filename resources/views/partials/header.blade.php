<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('index') }}">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('blog') }}">Blog</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin') }}">Admin</a>
      </li>
    </ul>
  </div>
</nav>